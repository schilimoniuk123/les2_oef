package com.example.android1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import java.text.BreakIterator;

public class HelloActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello);

        Intent i = getIntent();
        String name = i.getStringExtra("NAME");
        findViewById(R.id.lblName);
        //lblName.setText("hello " + name);
    }
}