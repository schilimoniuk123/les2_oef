package com.example.android1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MAIN activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //System.out.println("");
        int counter = 132;
        counter += 4;
        // Log.d(TAG,"oncreate log");

        Log.i(TAG, getString(R.string.logOnCreate_message));

        Toast.makeText(this, "yo phona cappn bruh", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onpause log");

    }

    public void handleClickBtnSend(View view)
    {
        //Toast.makeText(this, "button has been clicked", Toast.LENGTH_LONG).show();

        // TODO fix text ophalen
        TextView txtName = findViewById(R.id.txtName);
        String name = txtName.getText().toString();
        String name2 = ((TextView) findViewById(R.id.txtName)).getText().toString();
        //ga naar 2de activity
        Intent i = new Intent(this, HelloActivity.class);
        //geef de text mee naar de 2de activity
        i.putExtra("NAME", name);

        startActivity(i);


    }
}